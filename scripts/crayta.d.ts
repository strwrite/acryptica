
/** @noSelf */
declare function Printf(...args: any[]): void;

declare interface Character {
    GetName(this: Character): string;
}

interface PropertyDefBase {
    name: string 
    tooltip: string
    type: 'number' | 'string' | 'entity'
}

interface PropertyDefTyped<T> extends PropertyDefBase {
    default: T
}

interface PropertyDefNumber extends PropertyDefTyped<number> {
    type: 'number'
}

interface PropertyDefString extends PropertyDefTyped<string> {
    type: 'string'
}

interface PropertyDefEntity extends PropertyDefBase {
    type: 'entity'
}

type PropertyDef = PropertyDefNumber | PropertyDefString | PropertyDefEntity

type PropertyVal = string | number

type ScriptProps = {[key:string]: PropertyVal}

interface Script {
    properties: ScriptProps
}