import { earth, fire, voltage, water } from "./elements"
import { back, direct, go, left, me, right } from "./geometry"
import { time } from "./special"
import SpellIEffects from "./ieffects" 

export default class MagicSpell {
    private spell: string
    private geometry: Geometry = this.initialGeometry()
    private signs: { [key: string]: (self: MagicSpell) => void } = {
        //elements
        f: fire,
        w: water,
        v: voltage,
        e: earth,

        // geometry
        g: go,
        b: back,
        l: left,
        r: right,
        d: direct,
        i: me,

        // special
        t: time,
    }
    private elements: Array<Elemental> = []
    private phenomens: Array<Phenomen> = []
    private tail: Array<string> = []
    private ontail: Boolean = false
    private ongeometry = true
    private oneffects = false
    private ontime = false
    private token: Array<string> = []
    private spellGeometry: Geometry | undefined
    private elementsCollapsed?: ElementsCollapsed
    private elementsCombined?: ElementsCollapsed
    private phenomensLimit: number
    private _enteringIEffects: Boolean = false
    private iEffects?: SpellIEffects
    private time: number = 0

    public getOnTime(): Boolean {
        return this.ontime
    }

    public setMeTime() {
        this.growToken("me")
        //this.time = -1
    }

    public getEnteringIEffects(): Boolean {
        return this._enteringIEffects
    }

    public getResultingPhenomens(): Array<Phenomen> {
        return this.phenomens
    }

    initialGeometry() {
        return {
            points: [],
            lines: [],
            pointer: undefined
        }
    }

    constructor(spell: string, phenomensLimit: number) {
        this.spell = spell
        this.phenomensLimit = phenomensLimit
        this.parse()
        this.finalizePhenomen()
    }

    collapseElements() {
        if (!this.elements || this.elements.length <= 0) {
            return
        }
        const elements: ElementsCollapsed = {
            fire: undefined,
            water: undefined,
            earth: undefined,
            voltage: undefined
        }
        this.elements.forEach(element => {
            let currentCount = elements[element.word]
            if (currentCount == undefined) {
                currentCount = {
                    word: element.word,
                    count: 0
                }
            }
            currentCount.count++
            elements[element.word] = currentCount
        })
        if (elements.fire && elements.water) {
            delete elements.fire
            delete elements.water
        }
        if (elements.earth && elements.voltage) {
            delete elements.voltage
        }
        this.elementsCollapsed = elements
    }

    combine(multiplier: ElementCollapsed, addicter: ElementCollapsed): ElementCollapsed {
        return {
            word: multiplier.word,
            count: multiplier.count * (multiplier.count + addicter.count)
        }
    }

    combineElements() {
        if (!this.elementsCollapsed) {
            return
        }


        const elements = this.elementsCollapsed
        this.elementsCombined = {
            fire: elements.fire && elements.earth
                ? this.combine(elements.fire, elements.earth) : elements.fire,
            water: elements.water,
            earth: elements.earth,
            voltage: elements.voltage && elements.water
                ? this.combine(elements.voltage, elements.water)
                : elements.voltage
        }
    }

    finalizeElements() {
        this.collapseElements()
        this.combineElements()
    }

    finalizePhenomen() {
        this.finalizeElements()
        this.finalizeGeometry()
        if (this.phenomens.length < this.phenomensLimit) {
            const phenomen: Phenomen = {
                elements: this.elementsCombined,
                geometry: this.spellGeometry,
                token: this.token,
                time: this.time + 1,
                iEffect: this.iEffects && this.iEffects.getEffectType(),
                elementalShieldType: this.iEffects && this.iEffects.getElement(),
            }
            if (phenomen.elements || phenomen.iEffect) {
                this.phenomens.push(phenomen)
            }
        }
        if (this.phenomens.length == this.phenomensLimit) {
            // just reached the limit
            // everything after last phenomen would go to tail
            this.ontail = true
            this.ontime = false 
            this.ongeometry = false 
            this.oneffects = false
        }
        this.elements = []
        this.elementsCollapsed = undefined
        this.elementsCombined = undefined
        this.ongeometry = true
        this.oneffects = false
        this.ontime = false
        this.token = []
        this.geometry = this.initialGeometry()
        this.spellGeometry = undefined
        this._enteringIEffects = false
        this.time = 0
    }

    expectingTime() {
        return this.ongeometry || this.oneffects || this.ontime || this._enteringIEffects
    }

    expectingElement() {
        return this.ongeometry || this.oneffects
    }

    expectingGeometry() {
        return this.ongeometry
    }

    growToken(sign: string) {
        this.token.push(sign)
    }

    growTail(sign: string) {
        if (this.ontail) {
            this.tail.push(sign)
            this.growToken(sign)
            return true
        }
        return false
    }

    finalizePoint() {
        const pointer = this.geometry.pointer
        if (pointer == undefined) {
            return
        }
        const newPoint = {
            x: pointer.x,
            y: pointer.y
        }
        // skipping the last point if it's the same
        const last = this.geometry.points[this.geometry.points.length - 1]
        if (last && last.x == newPoint.x && last.y == newPoint.y) {
            return
        }
        // inserting new point to temporary geometry
        this.geometry.points.push(newPoint)
    }

    addTime() {
        const word = "time"
        if (this.growTail(word)) {
            return
        }
        if (!this.expectingTime()) {
            this.finalizePhenomen()
            // finalizing phenomen could move us to tail, so need to check again
            if (this.growTail(word)) {
                return
            }
        }
        if (this.ongeometry) {
            this.finalizeGeometry()
            this.ongeometry = false
        }        
        if (this.oneffects) {
            this.finalizeElements()
            this.oneffects = false
        }
        this.ontime = true
        this.growToken(word)
        this.time++
    }

    addGeometry(word: GeometryWords) {
        if (this.growTail(word)) {
            return
        }
        if (!this.expectingGeometry()) {
            this.finalizePhenomen()
            // finalizing phenomen could move us to tail, so need to check again
            if (this.growTail(word)) {
                return
            }
        }
        this.growToken(word)
        if (word == "me" && this.geometry.points.length == 0) {
            this._enteringIEffects = true
            this.ongeometry = false 
            return
        }
        if (word == "direct") {
            this.finalizePoint()
            return
        }
        const pointer: Point = this.geometry.pointer ? this.geometry.pointer : {
            x: 0,
            y: 0
        }
        switch (word) {
            case "go":
                pointer.y++
                break
            case "right":
                pointer.x++
                break
            case "left":
                pointer.x--
                break
            case "back":
                pointer.y--
                break
            case "me":
                pointer.x = 0
                pointer.y = 0
                break
        }
        this.geometry.pointer = pointer
    }

    finalizeLine() {
        this.finalizePoint()
        if (this.geometry.points.length > 0) {
            this.geometry.lines.push({
                points: this.geometry.points
            })
            const last = this.geometry.points[this.geometry.points.length]
            if (last) {
                this.geometry.points = [last]
            }
        }
    }

    finalizeGeometry() {
        // spellGeometry keeps already finalized geometry
        if (this.spellGeometry) {
            return
        }
        this.finalizeLine()
        this.ongeometry = false
        this.spellGeometry = this.geometry
        this.geometry = this.initialGeometry()
    }

    addElement(word: ElementWords) {
        if (this.growTail(word)) {
            return
        }
        if (!this.expectingElement()) {
            this.finalizePhenomen()
            if (this.growTail(word)) {
                return
            }
        }
        this.growToken(word)
        if (this.ongeometry) {
            this.finalizeGeometry()
            this.oneffects = true
        }
        this.elements.push({
            word: word
        })
    }

    addIEffect(word: IEffectWords) {
        if (this.growTail(word)) {
            return
        }
        this.growToken(word)
        const iEffects = this.iEffects ? this.iEffects : new SpellIEffects()
        iEffects.add(word)
        this.iEffects = iEffects
    }

    addElementalShield(word: IEffectElementalShields) {
        if (this.growTail(word)) {
            return
        }
        this.growToken(word)
        const iEffects = this.iEffects ? this.iEffects : new SpellIEffects()
        iEffects.addElementalShield(word)
        this.iEffects = iEffects
    }

    parse() {
        const spell = this.spell
        for (let i = 0; i < spell.length; i++) {
            const stateChanger = this.signs[spell[i]]
            if (stateChanger) {
                stateChanger(this)
            }
        }
    }

    print() {
        Printf("spell text: {1}", this.spell)

        this.phenomens.forEach((p, index) => {
            Printf("{1} phenomen:", index)
            if (p.elements && p.elements?.earth) {
                Printf("--earth: {1}", p.elements?.earth?.count)
            }
            if (p.elements && p.elements?.fire) {
                Printf("--fire: {1}", p.elements?.fire?.count)
            }
            if (p.elements && p.elements?.water) {
                Printf("--water: {1}", p.elements?.water?.count)
            }
            if (p.elements && p.elements?.voltage) {
                Printf("--voltage: {1}", p.elements?.voltage?.count)
            }
            if (p.geometry) {
                p.geometry?.lines.forEach((l, index) => {
                    Printf("--{1} line:", index)
                    l.points.forEach(po => {
                        Printf("----point: {1}, {2}", po.x, po.y)
                    })
                })
            }
        })
    }

}
