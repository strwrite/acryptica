import MagicSpell from "./spell"

export function go(spell: MagicSpell) {
    if (spell.getEnteringIEffects()) {
        spell.addIEffect("cognitive")
        return
    }
    spell.addGeometry("go")
}

export function direct(spell: MagicSpell) {
    spell.addGeometry("direct")
}

export function me(spell: MagicSpell) {
    if (spell.getOnTime()) {
        spell.setMeTime()
        return 
    }
    spell.addGeometry("me")
}

export function back(spell: MagicSpell) {
    if (spell.getEnteringIEffects()) {
        spell.addIEffect("physical")
        return
    }
    spell.addGeometry("back")
}

export function right(spell: MagicSpell) {
    if (spell.getEnteringIEffects()) {
        spell.addIEffect("outward")
        return
    }
    spell.addGeometry("right")
}

export function left(spell: MagicSpell) {
    if (spell.getEnteringIEffects()) {
        spell.addIEffect("inward")
        return
    }
    spell.addGeometry("left")
}
