export default class SpellIEffects {
    private first?: IEffectWords
    private effectType?: IEffectType
    private element?: IEffectElementalShields
    
    getEffectType(): IEffectType | undefined {
        return this.effectType
    }
    
    getElement(): IEffectElementalShields | undefined {
        return this.element
    }

    addElementalShield(word: IEffectElementalShields) {
        if (this.effectType == "ElementsShield") {
            this.element = word
        }
    }

    add(word: IEffectWords) {
        if (this.effectType) {
            return 
        }
        if (!this.first) {
            this.first = word
            return
        }

        switch (this.first) {
            case "cognitive":
                this.cognitive(word)
                break
            case "inward":
                this.inward(word)
                break
            case "outward":
                this.outward(word)
                break
            case "physical":
                this.physical(word)
                break
        }
    }

    inward(word: IEffectWords) {
        switch (word) {
            case "cognitive":
                this.effectType = "EnergyRestoration"
                return
            case "physical":
                this.effectType = "HealthAmplification"
                return
        }
    }

    outward(word: IEffectWords) {
        switch (word) {
            case "cognitive":
                this.effectType = "ElementsShield"
                return
            case "physical":
                this.effectType = "PhysicalShield"
                return
        }
    }

    physical(word: IEffectWords) {
        switch (word) {
            case "inward":
                this.effectType = "SpeedAmplification"
                return
            case "outward":
                this.effectType = "JumpApmplification"
                return
        }
    }

    cognitive(word: IEffectWords) {
        switch (word) {
            case "inward":
                this.effectType = "DeepSight"
                return
            case "outward":
                this.effectType = "InterferenceDetection"
                return
        }
    }
}