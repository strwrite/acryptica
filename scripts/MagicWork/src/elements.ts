import MagicSpell from "./spell"

export function fire(spell: MagicSpell) {
    if (spell.getEnteringIEffects()) {
        spell.addElementalShield("fire")
        return
    }
    spell.addElement("fire")
}

export function voltage(spell: MagicSpell) {
    if (spell.getEnteringIEffects()) {
        spell.addElementalShield("voltage")
        return
    }
    spell.addElement("voltage")
}

export function earth(spell: MagicSpell) {
    if (spell.getEnteringIEffects()) {
        spell.addElementalShield("earth")
        return
    }
    spell.addElement("earth")
}

export function water(spell: MagicSpell) {
    if (spell.getEnteringIEffects()) {
        spell.addElementalShield("water")
        return
    }
    spell.addElement("water")
}