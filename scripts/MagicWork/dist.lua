
local ____modules = {}
local ____moduleCache = {}
local ____originalRequire = require
local function require(file)
    if ____moduleCache[file] then
        return ____moduleCache[file]
    end
    if ____modules[file] then
        ____moduleCache[file] = ____modules[file]()
        return ____moduleCache[file]
    else
        if ____originalRequire then
            return ____originalRequire(file)
        else
            error("module '" .. file .. "' not found")
        end
    end
end
____modules = {
["src.utils"] = function() --[[ Generated with https://github.com/TypeScriptToLua/TypeScriptToLua ]]
local ____exports = {}
function ____exports.isNumber(self, x)
    return type(x) == "number"
end
function ____exports.isString(self, x)
    return type(x) == "string"
end
return ____exports
end,
["src.elements"] = function() --[[ Generated with https://github.com/TypeScriptToLua/TypeScriptToLua ]]
local ____exports = {}
function ____exports.fire(self, spell)
    if spell:getEnteringIEffects() then
        spell:addElementalShield("fire")
        return
    end
    spell:addElement("fire")
end
function ____exports.voltage(self, spell)
    if spell:getEnteringIEffects() then
        spell:addElementalShield("voltage")
        return
    end
    spell:addElement("voltage")
end
function ____exports.earth(self, spell)
    if spell:getEnteringIEffects() then
        spell:addElementalShield("earth")
        return
    end
    spell:addElement("earth")
end
function ____exports.water(self, spell)
    if spell:getEnteringIEffects() then
        spell:addElementalShield("water")
        return
    end
    spell:addElement("water")
end
return ____exports
end,
["src.geometry"] = function() --[[ Generated with https://github.com/TypeScriptToLua/TypeScriptToLua ]]
local ____exports = {}
function ____exports.go(self, spell)
    if spell:getEnteringIEffects() then
        spell:addIEffect("cognitive")
        return
    end
    spell:addGeometry("go")
end
function ____exports.direct(self, spell)
    spell:addGeometry("direct")
end
function ____exports.me(self, spell)
    if spell:getOnTime() then
        spell:setMeTime()
        return
    end
    spell:addGeometry("me")
end
function ____exports.back(self, spell)
    if spell:getEnteringIEffects() then
        spell:addIEffect("physical")
        return
    end
    spell:addGeometry("back")
end
function ____exports.right(self, spell)
    if spell:getEnteringIEffects() then
        spell:addIEffect("outward")
        return
    end
    spell:addGeometry("right")
end
function ____exports.left(self, spell)
    if spell:getEnteringIEffects() then
        spell:addIEffect("inward")
        return
    end
    spell:addGeometry("left")
end
return ____exports
end,
["src.special"] = function() --[[ Generated with https://github.com/TypeScriptToLua/TypeScriptToLua ]]
local ____exports = {}
function ____exports.time(self, spell)
    spell:addTime()
end
return ____exports
end,
["src.ieffects"] = function() --[[ Generated with https://github.com/TypeScriptToLua/TypeScriptToLua ]]
-- Lua Library inline imports
function __TS__Class(self)
    local c = {prototype = {}}
    c.prototype.__index = c.prototype
    c.prototype.constructor = c
    return c
end

local ____exports = {}
____exports.default = (function()
    ____exports.default = __TS__Class()
    local SpellIEffects = ____exports.default
    SpellIEffects.name = "SpellIEffects"
    function SpellIEffects.prototype.____constructor(self)
    end
    function SpellIEffects.prototype.getEffectType(self)
        return self.effectType
    end
    function SpellIEffects.prototype.getElement(self)
        return self.element
    end
    function SpellIEffects.prototype.addElementalShield(self, word)
        if self.effectType == "ElementsShield" then
            self.element = word
        end
    end
    function SpellIEffects.prototype.add(self, word)
        if self.effectType then
            return
        end
        if not self.first then
            self.first = word
            return
        end
        local ____switch11 = self.first
        if ____switch11 == "cognitive" then
            goto ____switch11_case_0
        elseif ____switch11 == "inward" then
            goto ____switch11_case_1
        elseif ____switch11 == "outward" then
            goto ____switch11_case_2
        elseif ____switch11 == "physical" then
            goto ____switch11_case_3
        end
        goto ____switch11_end
        ::____switch11_case_0::
        do
            self:cognitive(word)
            goto ____switch11_end
        end
        ::____switch11_case_1::
        do
            self:inward(word)
            goto ____switch11_end
        end
        ::____switch11_case_2::
        do
            self:outward(word)
            goto ____switch11_end
        end
        ::____switch11_case_3::
        do
            self:physical(word)
            goto ____switch11_end
        end
        ::____switch11_end::
    end
    function SpellIEffects.prototype.inward(self, word)
        local ____switch13 = word
        if ____switch13 == "cognitive" then
            goto ____switch13_case_0
        elseif ____switch13 == "physical" then
            goto ____switch13_case_1
        end
        goto ____switch13_end
        ::____switch13_case_0::
        do
            self.effectType = "EnergyRestoration"
            return
        end
        ::____switch13_case_1::
        do
            self.effectType = "HealthAmplification"
            return
        end
        ::____switch13_end::
    end
    function SpellIEffects.prototype.outward(self, word)
        local ____switch15 = word
        if ____switch15 == "cognitive" then
            goto ____switch15_case_0
        elseif ____switch15 == "physical" then
            goto ____switch15_case_1
        end
        goto ____switch15_end
        ::____switch15_case_0::
        do
            self.effectType = "ElementsShield"
            return
        end
        ::____switch15_case_1::
        do
            self.effectType = "PhysicalShield"
            return
        end
        ::____switch15_end::
    end
    function SpellIEffects.prototype.physical(self, word)
        local ____switch17 = word
        if ____switch17 == "inward" then
            goto ____switch17_case_0
        elseif ____switch17 == "outward" then
            goto ____switch17_case_1
        end
        goto ____switch17_end
        ::____switch17_case_0::
        do
            self.effectType = "SpeedAmplification"
            return
        end
        ::____switch17_case_1::
        do
            self.effectType = "JumpApmplification"
            return
        end
        ::____switch17_end::
    end
    function SpellIEffects.prototype.cognitive(self, word)
        local ____switch19 = word
        if ____switch19 == "inward" then
            goto ____switch19_case_0
        elseif ____switch19 == "outward" then
            goto ____switch19_case_1
        end
        goto ____switch19_end
        ::____switch19_case_0::
        do
            self.effectType = "DeepSight"
            return
        end
        ::____switch19_case_1::
        do
            self.effectType = "InterferenceDetection"
            return
        end
        ::____switch19_end::
    end
    return SpellIEffects
end)()
return ____exports
end,
["src.spell"] = function() --[[ Generated with https://github.com/TypeScriptToLua/TypeScriptToLua ]]
-- Lua Library inline imports
function __TS__Class(self)
    local c = {prototype = {}}
    c.prototype.__index = c.prototype
    c.prototype.constructor = c
    return c
end

function __TS__ArrayForEach(arr, callbackFn)
    do
        local i = 0
        while i < #arr do
            callbackFn(_G, arr[i + 1], i, arr)
            i = i + 1
        end
    end
end

function __TS__ObjectGetOwnPropertyDescriptors(object)
    local metatable = getmetatable(object)
    if not metatable then
        return {}
    end
    return rawget(metatable, "_descriptors")
end

function __TS__Delete(target, key)
    local descriptors = __TS__ObjectGetOwnPropertyDescriptors(target)
    local descriptor = descriptors[key]
    if descriptor then
        if not descriptor.configurable then
            error(
                ((("Cannot delete property " .. tostring(key)) .. " of ") .. tostring(target)) .. ".",
                0
            )
        end
        descriptors[key] = nil
        return true
    end
    if target[key] ~= nil then
        target[key] = nil
        return true
    end
    return false
end

function __TS__ArrayPush(arr, ...)
    local items = {...}
    for ____, item in ipairs(items) do
        arr[#arr + 1] = item
    end
    return #arr
end

function __TS__New(target, ...)
    local instance = setmetatable({}, target.prototype)
    instance:____constructor(...)
    return instance
end

function __TS__StringAccess(self, index)
    if (index >= 0) and (index < #self) then
        return string.sub(self, index + 1, index + 1)
    end
end

local ____exports = {}
local ____elements = require("src.elements")
local earth = ____elements.earth
local fire = ____elements.fire
local voltage = ____elements.voltage
local water = ____elements.water
local ____geometry = require("src.geometry")
local back = ____geometry.back
local direct = ____geometry.direct
local go = ____geometry.go
local left = ____geometry.left
local me = ____geometry.me
local right = ____geometry.right
local ____special = require("src.special")
local time = ____special.time
local ____ieffects = require("src.ieffects")
local SpellIEffects = ____ieffects.default
____exports.default = (function()
    ____exports.default = __TS__Class()
    local MagicSpell = ____exports.default
    MagicSpell.name = "MagicSpell"
    function MagicSpell.prototype.____constructor(self, spell, phenomensLimit)
        self.geometry = self:initialGeometry()
        self.signs = {f = fire, w = water, v = voltage, e = earth, g = go, b = back, l = left, r = right, d = direct, i = me, t = time}
        self.elements = {}
        self.phenomens = {}
        self.tail = {}
        self.ontail = false
        self.ongeometry = true
        self.oneffects = false
        self.ontime = false
        self.token = {}
        self._enteringIEffects = false
        self.time = 0
        self.spell = spell
        self.phenomensLimit = phenomensLimit
        self:parse()
        self:finalizePhenomen()
    end
    function MagicSpell.prototype.getOnTime(self)
        return self.ontime
    end
    function MagicSpell.prototype.setMeTime(self)
        self:growToken("me")
    end
    function MagicSpell.prototype.getEnteringIEffects(self)
        return self._enteringIEffects
    end
    function MagicSpell.prototype.getResultingPhenomens(self)
        return self.phenomens
    end
    function MagicSpell.prototype.initialGeometry(self)
        return {points = {}, lines = {}, pointer = nil}
    end
    function MagicSpell.prototype.collapseElements(self)
        if (not self.elements) or (#self.elements <= 0) then
            return
        end
        local elements = {fire = nil, water = nil, earth = nil, voltage = nil}
        __TS__ArrayForEach(
            self.elements,
            function(____, element)
                local currentCount = elements[element.word]
                if currentCount == nil then
                    currentCount = {word = element.word, count = 0}
                end
                currentCount.count = currentCount.count + 1
                elements[element.word] = currentCount
            end
        )
        if elements.fire and elements.water then
            __TS__Delete(elements, "fire")
            __TS__Delete(elements, "water")
        end
        if elements.earth and elements.voltage then
            __TS__Delete(elements, "voltage")
        end
        self.elementsCollapsed = elements
    end
    function MagicSpell.prototype.combine(self, multiplier, addicter)
        return {word = multiplier.word, count = multiplier.count * (multiplier.count + addicter.count)}
    end
    function MagicSpell.prototype.combineElements(self)
        if not self.elementsCollapsed then
            return
        end
        local elements = self.elementsCollapsed
        self.elementsCombined = {
            fire = ((elements.fire and elements.earth) and self:combine(elements.fire, elements.earth)) or elements.fire,
            water = elements.water,
            earth = elements.earth,
            voltage = ((elements.voltage and elements.water) and self:combine(elements.voltage, elements.water)) or elements.voltage
        }
    end
    function MagicSpell.prototype.finalizeElements(self)
        self:collapseElements()
        self:combineElements()
    end
    function MagicSpell.prototype.finalizePhenomen(self)
        self:finalizeElements()
        self:finalizeGeometry()
        if #self.phenomens < self.phenomensLimit then
            local phenomen = {
                elements = self.elementsCombined,
                geometry = self.spellGeometry,
                token = self.token,
                time = self.time + 1,
                iEffect = self.iEffects and self.iEffects:getEffectType(),
                elementalShieldType = self.iEffects and self.iEffects:getElement()
            }
            if phenomen.elements or phenomen.iEffect then
                __TS__ArrayPush(self.phenomens, phenomen)
            end
        end
        if #self.phenomens == self.phenomensLimit then
            self.ontail = true
            self.ontime = false
            self.ongeometry = false
            self.oneffects = false
        end
        self.elements = {}
        self.elementsCollapsed = nil
        self.elementsCombined = nil
        self.ongeometry = true
        self.oneffects = false
        self.ontime = false
        self.token = {}
        self.geometry = self:initialGeometry()
        self.spellGeometry = nil
        self._enteringIEffects = false
        self.time = 0
    end
    function MagicSpell.prototype.expectingTime(self)
        return ((self.ongeometry or self.oneffects) or self.ontime) or self._enteringIEffects
    end
    function MagicSpell.prototype.expectingElement(self)
        return self.ongeometry or self.oneffects
    end
    function MagicSpell.prototype.expectingGeometry(self)
        return self.ongeometry
    end
    function MagicSpell.prototype.growToken(self, sign)
        __TS__ArrayPush(self.token, sign)
    end
    function MagicSpell.prototype.growTail(self, sign)
        if self.ontail then
            __TS__ArrayPush(self.tail, sign)
            self:growToken(sign)
            return true
        end
        return false
    end
    function MagicSpell.prototype.finalizePoint(self)
        local pointer = self.geometry.pointer
        if pointer == nil then
            return
        end
        local newPoint = {x = pointer.x, y = pointer.y}
        local last = self.geometry.points[#self.geometry.points]
        if (last and (last.x == newPoint.x)) and (last.y == newPoint.y) then
            return
        end
        __TS__ArrayPush(self.geometry.points, newPoint)
    end
    function MagicSpell.prototype.addTime(self)
        local word = "time"
        if self:growTail(word) then
            return
        end
        if not self:expectingTime() then
            self:finalizePhenomen()
            if self:growTail(word) then
                return
            end
        end
        if self.ongeometry then
            self:finalizeGeometry()
            self.ongeometry = false
        end
        if self.oneffects then
            self:finalizeElements()
            self.oneffects = false
        end
        self.ontime = true
        self:growToken(word)
        self.time = self.time + 1
    end
    function MagicSpell.prototype.addGeometry(self, word)
        if self:growTail(word) then
            return
        end
        if not self:expectingGeometry() then
            self:finalizePhenomen()
            if self:growTail(word) then
                return
            end
        end
        self:growToken(word)
        if (word == "me") and (#self.geometry.points == 0) then
            self._enteringIEffects = true
            self.ongeometry = false
            return
        end
        if word == "direct" then
            self:finalizePoint()
            return
        end
        local pointer = (self.geometry.pointer and self.geometry.pointer) or ({x = 0, y = 0})
        local ____switch44 = word
        if ____switch44 == "go" then
            goto ____switch44_case_0
        elseif ____switch44 == "right" then
            goto ____switch44_case_1
        elseif ____switch44 == "left" then
            goto ____switch44_case_2
        elseif ____switch44 == "back" then
            goto ____switch44_case_3
        elseif ____switch44 == "me" then
            goto ____switch44_case_4
        end
        goto ____switch44_end
        ::____switch44_case_0::
        do
            pointer.y = pointer.y + 1
            goto ____switch44_end
        end
        ::____switch44_case_1::
        do
            pointer.x = pointer.x + 1
            goto ____switch44_end
        end
        ::____switch44_case_2::
        do
            pointer.x = pointer.x - 1
            goto ____switch44_end
        end
        ::____switch44_case_3::
        do
            pointer.y = pointer.y - 1
            goto ____switch44_end
        end
        ::____switch44_case_4::
        do
            pointer.x = 0
            pointer.y = 0
            goto ____switch44_end
        end
        ::____switch44_end::
        self.geometry.pointer = pointer
    end
    function MagicSpell.prototype.finalizeLine(self)
        self:finalizePoint()
        if #self.geometry.points > 0 then
            __TS__ArrayPush(self.geometry.lines, {points = self.geometry.points})
            local last = self.geometry.points[#self.geometry.points + 1]
            if last then
                self.geometry.points = {last}
            end
        end
    end
    function MagicSpell.prototype.finalizeGeometry(self)
        if self.spellGeometry then
            return
        end
        self:finalizeLine()
        self.ongeometry = false
        self.spellGeometry = self.geometry
        self.geometry = self:initialGeometry()
    end
    function MagicSpell.prototype.addElement(self, word)
        if self:growTail(word) then
            return
        end
        if not self:expectingElement() then
            self:finalizePhenomen()
            if self:growTail(word) then
                return
            end
        end
        self:growToken(word)
        if self.ongeometry then
            self:finalizeGeometry()
            self.oneffects = true
        end
        __TS__ArrayPush(self.elements, {word = word})
    end
    function MagicSpell.prototype.addIEffect(self, word)
        if self:growTail(word) then
            return
        end
        self:growToken(word)
        local iEffects = (self.iEffects and self.iEffects) or __TS__New(SpellIEffects)
        iEffects:add(word)
        self.iEffects = iEffects
    end
    function MagicSpell.prototype.addElementalShield(self, word)
        if self:growTail(word) then
            return
        end
        self:growToken(word)
        local iEffects = (self.iEffects and self.iEffects) or __TS__New(SpellIEffects)
        iEffects:addElementalShield(word)
        self.iEffects = iEffects
    end
    function MagicSpell.prototype.parse(self)
        local spell = self.spell
        do
            local i = 0
            while i < #spell do
                local stateChanger = self.signs[__TS__StringAccess(spell, i)]
                if stateChanger then
                    stateChanger(nil, self)
                end
                i = i + 1
            end
        end
    end
    function MagicSpell.prototype.print(self)
        Printf("spell text: {1}", self.spell)
        __TS__ArrayForEach(
            self.phenomens,
            function(____, p, index)
                Printf("{1} phenomen:", index)
                if p.elements and p.elements.earth then
                    Printf("--earth: {1}", p.elements.earth.count)
                end
                if p.elements and p.elements.fire then
                    Printf("--fire: {1}", p.elements.fire.count)
                end
                if p.elements and p.elements.water then
                    Printf("--water: {1}", p.elements.water.count)
                end
                if p.elements and p.elements.voltage then
                    Printf("--voltage: {1}", p.elements.voltage.count)
                end
                if p.geometry then
                    __TS__ArrayForEach(
                        p.geometry.lines,
                        function(____, l, index)
                            Printf("--{1} line:", index)
                            __TS__ArrayForEach(
                                l.points,
                                function(____, po)
                                    Printf("----point: {1}, {2}", po.x, po.y)
                                end
                            )
                        end
                    )
                end
            end
        )
    end
    return MagicSpell
end)()
return ____exports
end,
["index"] = function() --[[ Generated with https://github.com/TypeScriptToLua/TypeScriptToLua ]]
-- Lua Library inline imports
function __TS__New(target, ...)
    local instance = setmetatable({}, target.prototype)
    instance:____constructor(...)
    return instance
end

local ____exports = {}
local ____utils = require("src.utils")
local isNumber = ____utils.isNumber
local ____spell = require("src.spell")
local MagicSpell = ____spell.default
____exports.Properties = {{name = "phenomensLimit", tooltip = "Number of phenomens allowed in a spell", type = "number", default = 3}}
function ____exports.Init(self)
end
function ____exports.Translate(self, spell)
    local phenomensLimit = self.properties.phenomensLimit
    if not isNumber(nil, phenomensLimit) then
        return
    end
    local magic = __TS__New(MagicSpell, spell, phenomensLimit)
    return magic
end
Printf("Test spell parsing:")
__TS__New(MagicSpell, "ggdirrfe", 3):print()
return ____exports
end,
}
return require("index")
