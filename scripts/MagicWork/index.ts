import {isNumber} from "./src/utils"
import MagicSpell from "./src/spell"

export const Properties: Array<PropertyDef> = [{
    name: "phenomensLimit",
    tooltip: "Number of phenomens allowed in a spell",
    type: 'number',
    default: 3,
}]

export function Init(this: Script) {
}

export function Translate(this: Script, spell: string) {
    const phenomensLimit = this.properties["phenomensLimit"]
    if (!isNumber(phenomensLimit)) {
        return
    }
    const magic = new MagicSpell(spell, phenomensLimit)
    return magic
}


Printf("Test spell parsing:")
// new MagicSpell("ggf", 3).print()
// new MagicSpell("ggfrve", 3).print()
new MagicSpell("ggdirrfe", 3).print()
// new MagicSpell("ff", 3).print()