import {describe, expect, test} from '@jest/globals'
import MagicSpell from "./../MagicWork/src/spell"

test('spell: go right', () => {
  let phenomens: Array<Phenomen> = []
  expect(new MagicSpell("gr", 3).getResultingPhenomens()).toStrictEqual(phenomens);
})

test('spell: I right go fire time time', () => {
  let phenomens: Array<Phenomen> = [{
    elements: undefined,
    token: [
      "me", "outward", "cognitive", "fire", "time", "time"
    ],
    geometry: {
      lines: [],
      pointer: undefined,
      points: []
    },
    time: 3,
    iEffect: "ElementsShield",
    elementalShieldType: "fire"
  }]
  expect(new MagicSpell("irgftt", 3).getResultingPhenomens()).toStrictEqual(phenomens);
})

test('spell: I go right time time time', () => {
  let phenomens: Array<Phenomen> = [{
    elements: undefined,
    token: [
      "me", "cognitive", "outward", "time", "time", "time"
    ],
    geometry: {
      lines: [],
      pointer: undefined,
      points: []
    },
    time: 4,
    iEffect: "InterferenceDetection",
    elementalShieldType: undefined
  }]
  expect(new MagicSpell("igrttt", 3).getResultingPhenomens()).toStrictEqual(phenomens);
})

test('spell: go go direct I right right fire earth', () => {
  let phenomens: Array<Phenomen> = [{
    elements: {
      fire: {
        word: "fire",
        count: 2
      },
      earth: {
        word: "earth",
        count: 1
      },
      voltage: undefined,
      water: undefined
    },
    token: [
      "go", "go", "direct", "me", "right", "right", "fire", "earth"
    ],
    geometry: {
      lines: [ { points: [ { y: 2, x: 0 }, { y: 0, x: 2 } ] } ],
      points: [ { y: 2, x: 0 }, { y: 0, x: 2 } ],
      pointer: {
        y: 0, 
        x: 2,
      }
    },
    time: 1,
    iEffect: undefined,
    elementalShieldType: undefined
  }]
  expect(new MagicSpell("ggdirrfe", 3).getResultingPhenomens()).toStrictEqual(phenomens);
})

test('spell: go go fire', () => {
    let phenomens: Array<Phenomen> = [{
      elements: {
        fire: {
          word: "fire",
          count: 1
        },
        earth: undefined,
        voltage: undefined,
        water: undefined
      },
      token: [
        "go", "go", "fire"
      ],
      geometry: {
        lines: [ { points: [ { y: 2, x: 0 } ] } ],
        points: [ { y: 2, x: 0 } ],
        pointer: {
          x: 0,
          y: 2,
        }
      },
      time: 1,
      iEffect: undefined,
      elementalShieldType: undefined
    }]
    expect(new MagicSpell("ggf", 3).getResultingPhenomens()).toStrictEqual(phenomens);
})

test('spell: go go fire right voltage earth', () => {
  let expected: Array<Phenomen> = [{
    token: [
      "go", "go", "fire"
    ],
    elements: {
      fire: {
        word: "fire",
        count: 1
      },
      earth: undefined,
      voltage: undefined,
      water: undefined
    },
    geometry: {
      lines: [ { points: [ { y: 2, x: 0 } ] } ],
      points: [ { y: 2, x: 0 } ],
      pointer: {
        x: 0,
        y: 2,
      }
    },
    time: 1,
    iEffect: undefined,
    elementalShieldType: undefined
  },{
    token: [
      "right", "voltage", "earth"
    ],
    elements: {
      earth: {
        word: "earth",
        count: 1
      },
      fire: undefined,
      voltage: undefined,
      water: undefined
    },
    geometry: {
      lines: [ { points: [ { y: 0, x: 1 } ] } ],
      points: [ { y: 0, x: 1 } ],
      pointer: {
        x: 1,
        y: 0,
      }
    },
    time: 1,
    iEffect: undefined,
    elementalShieldType: undefined
  }]
  expect(new MagicSpell("ggfrve", 3).getResultingPhenomens()).toStrictEqual(expected)
})