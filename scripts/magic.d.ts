interface Point {
    x: number 
    y: number
}

interface Line {
    points: Array<Point>
}

type GeometryWords = 'direct' | 'me' | 'go' | 'right' | 'left' | 'back'

interface Geometry {
    points: Array<Point>
    lines: Array<Line>
    pointer?: Point
}

type ElementWords = 'fire' | 'water' | 'earth' | 'voltage'

interface Elemental {
    word: ElementWords
}

interface ElementCollapsed extends Elemental {
    count: number
}

interface ElementsCollapsed {
    fire?: ElementCollapsed
    water?: ElementCollapsed 
    earth?: ElementCollapsed 
    voltage?: ElementCollapsed 
}

type IEffectType =
    'DeepSight'
    | 'InterferenceDetection' 
    | 'JumpApmplification'
    | 'SpeedAmplification'
    | 'HealthAmplification'
    | 'EnergyRestoration'
    | 'PhysicalShield'
    | 'ElementsShield'

interface Phenomen {
    elements?: ElementsCollapsed
    token: Array<string>
    geometry?: Geometry
    time: number
    iEffect?: IEffectType
    elementalShieldType?: IEffectElementalShields
}

type IEffectWords = 'outward' | 'inward' | 'cognitive' | 'physical'
type IEffectElementalShields = ElementWords